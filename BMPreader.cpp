#include <fstream>
#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <iterator>

enum colors_t { blue, green, red };

struct Pixel{
	int blue;
	int green;
	int red;
	friend class BMPimg;
};

class BMPimg{
private:
	std::string name;
	int width;
	int height;
	std::vector<Pixel> data;
	unsigned char* info;
public:
	BMPimg();
	virtual ~BMPimg();
	BMPimg(const BMPimg&);
	BMPimg& operator=(const BMPimg&);
	BMPimg(const std::string&);
	bool create_file();
	Pixel operator()(int x, int y) const;
	std::vector<Pixel> get_5x5_surround_matrix(int x, int y);
	void kernel_convolute(const std::vector<double>&);
	void color_filter(colors_t);
};

BMPimg::BMPimg(){
	this->name = "none";
	this->width = 0;
	this->height = 0;
	this->info = nullptr;
}

BMPimg::~BMPimg(){
	delete[] info;
}

BMPimg::BMPimg(const BMPimg& source){
	this->name = source.name;
	this->height = source.height;
	this->width = source.width;
	this->data = source.data;

	try{
		this->info = new unsigned char[54];
	}
	catch (std::exception& e){
		std::cerr << "Constructor allocation failure" << std::endl;
		this->info = nullptr;
	}

	for (int i = 0; i < 54; i++){
		this->info[i] = source.info[i];
	}
}

BMPimg& BMPimg::operator=(const BMPimg& rhs){
	if (this != &rhs){
		delete[] this->info;

		this->data = rhs.data;
		this->name = rhs.name;
		this->height = rhs.height;
		this->width = rhs.width;

		try{
			this->info = new unsigned char[54];
		}
		catch (std::exception& e){
			std::cerr << "op= allocation failure" << std::endl;
			this->info = nullptr;
		}

		for (int i = 0; i < 54; i++){
			this->info[i] = rhs.info[i];
		}
	}
	return *this;
}

BMPimg::BMPimg(const std::string& file_name){
	this->name = file_name;
	FILE* img = fopen((file_name + ".bmp").c_str(), "rb");
	if (img == NULL)
		throw "Argument Exception";

	try{
		this->info = new unsigned char[54];
	}
	catch (std::exception& e){
		std::cerr << "Constructor allocation failure" << std::endl;
		this->info = nullptr;
	}

	//reads .bmp header to member array info
	fread(this->info, sizeof(unsigned char), 54, img);

	//obtain width and height from header info
	this->width = *(int*)&info[18];
	this->height = *(int*)&info[22];

	std::cout << std::endl;
	std::cout << "Name: " << file_name << std::endl;
	std::cout << "Width: " << this->width << std::endl;
	std::cout << "Height: " << this->height << std::endl << std::endl;

	//create temporary array to read each row of pixel data into
	int row_padded = (this->width * 3 + 3) & (~3);
	unsigned char* row_data;

	try{
		row_data = new unsigned char[row_padded];
	}
	catch (std::exception& e){
		std::cerr << "Constructor allocation failure" << std::endl;
		this->info = nullptr;
	}

	//read 1 row of pixels at a time, pushing each pixel's data into data vector
	for (int i = 0; i < this->height; i++){
		fread(row_data, sizeof(unsigned char), row_padded, img);
		for (int j = 0; j < this->width * 3; j += 3){
			Pixel pix;
			pix.blue = (int)row_data[j + 0];
			pix.green = (int)row_data[j + 1];
			pix.red = (int)row_data[j + 2];
			this->data.push_back(pix);
		}
	}

	delete[] row_data;
	fclose(img);
}

bool BMPimg::create_file(){
	FILE* new_img = fopen((this->name + "_filtered.bmp").c_str(), "wb");
	if (new_img == NULL)
		throw "Argument Exception";

	//write .bmp header to new file
	fwrite(this->info, sizeof(unsigned char), 54, new_img);

	//create temporary array of appropriate size to hold pixel + padding data
	int row_padding = 0;
	if ((this->width * 3) % 4 != 0){
		row_padding = 4 - ((this->width * 3) % 4);
	}

	int total_bytes = (this->width * 3 + row_padding) * this->height;
	unsigned char* temp_data;
	unsigned char pad = 0;

	try{
		temp_data = new unsigned char[total_bytes];
	}
	catch (std::exception& e){
		std::cerr << "Constructor allocation failure" << std::endl;
		this->info = nullptr;
	}

	//write from temporary array to new file
	int pixel_index = 0;
	int byte_index = 0;
	for (int i = 0; i < this->height; i++){
		for (int j = 0; j < this->width * 3; j += 3){
			temp_data[byte_index + 0] = (unsigned char)this->data[pixel_index].blue;
			temp_data[byte_index + 1] = (unsigned char)this->data[pixel_index].green;
			temp_data[byte_index + 2] = (unsigned char)this->data[pixel_index].red;

			pixel_index += 1;
			byte_index += 3;
		}
		for (int g = 0; g < row_padding; g++){
			temp_data[byte_index] = pad;
			byte_index++;
		}
	}

	fwrite(temp_data, sizeof(unsigned char), total_bytes, new_img);
	delete[] temp_data;
	fclose(new_img);
	return 1;
}

Pixel BMPimg::operator()(int x, int y) const{
	int xcoord, ycoord;
	
	/*Edge Case: there will soon be a problem generating 5x5 pixel matrices
				around each pixel since pixels on the edges are not completely surrounded.
				Account for this by effectively extending edge pixels to negative x/y-coord values.
	*/
	if (x < 0)
		xcoord = 0;
	else if (x >= this->width)
		xcoord = this->width - 1;
	else
		xcoord = x;

	if (y < 0)
		ycoord = 0;
	else if (y >= this->height)
		ycoord = this->height - 1;
	else
		ycoord = y;

	return this->data[(this->width * ycoord) + xcoord];
}

std::vector<Pixel> BMPimg::get_5x5_surround_matrix(int x, int y){
	if (x >= this->width || x < 0 || y >= this->height || y < 0){
		std::cout << "ERROR: 5x5 surround matrix paramaters out of bounds";
	}
	std::vector<Pixel> matrix;

	for (int i = -2; i < 3; i++){
		for (int j = -2; j < 3; j++){
			Pixel p = (*this)(x + j, y + i);
			matrix.push_back(p);
		}
	}
	return matrix;
}

void BMPimg::kernel_convolute(const std::vector<double>& kernel){
	if (kernel.size() != 25){
		std::cout << "ERROR: kernel vector not 5x5";
	}
	std::vector<Pixel> new_data;

	for (int y = 0; y < this->height; y++){
		for (int x = 0; x < this->width; x++){
			int newred = 0;
			int newgreen = 0;
			int newblue = 0;
			std::vector<Pixel> matrix = this->get_5x5_surround_matrix(x, y);

			for (int i = 0; i < 25; i++){
				newred += matrix[i].red * kernel[i];
				newgreen += matrix[i].green * kernel[i];
				newblue += matrix[i].blue * kernel[i];
			}
			//ensure new values are within bounds
			if (newred < 0)
				newred = 0;
			if (newred > 255)
				newred = 255;
			if (newblue < 0)
				newblue = 0;
			if (newblue > 255)
				newblue = 255;
			if (newgreen < 0)
				newgreen = 0;
			if (newgreen > 255)
				newgreen = 255;

			Pixel pix;
			pix.blue = newblue;
			pix.green = newgreen;
			pix.red = newred;
			new_data.push_back(pix);
		}
	}

	this->data = new_data;
	return;
}

void BMPimg::color_filter(colors_t col){
	//algorithm and lambda function
	std::for_each(this->data.begin(), this->data.end(), [&](Pixel& p){if (col == 0) p.blue = 255;
	else if (col == 1) p.green = 255;
	else p.red = 255; });
}


int main(){
	std::string original_file;
	std::cout << "Enter name of BMP image file:\n";
	std::cin >> original_file;

	std::vector<double> sharpen_kernel{ 0, 0, 0, 0, 0,
										0, -1, -1, -1, 0,
										0, -1, 9, -1, 0,
										0, -1, -1, -1, 0,
										0, 0, 0, 0, 0 };

	std::vector<double> edge_kernel{ 0, 0, 0, 0, 0,
									 0, 0, 1, 0, 0,
									 0, 1, -4, 1, 0,
									 0, 0, 1, 0, 0,
									 0, 0, 0, 0, 0 };

	std::vector<double> blur_kernel{ 0, 0, 0, 0, 0,
									0, 1.0 / 9, 1.0 / 9, 1.0 / 9, 0,
									0, 1.0 / 9, 1.0 / 9, 1.0 / 9, 0,
									0, 1.0 / 9, 1.0 / 9, 1.0 / 9, 0,
									0, 0, 0, 0, 0 };


	BMPimg a(original_file);

	std::string ans;
	std::cout << "Select color filter \n(type blue, green, red, or none):\n\n";
	std::cin >> ans;
	if (ans.substr(0, 1) == "b")
		a.color_filter(blue);
	else if (ans.substr(0, 1) == "g")
		a.color_filter(green);
	else if (ans.substr(0, 1) == "r")
		a.color_filter(red);

	std::cout << "Select kernel filter for convolution\n(type edge, blur, sharpen, or none):\n\n";
	std::cin >> ans;
	std::cout << "Generating image...\n";
	if (ans.substr(0, 1) == "e")
		a.kernel_convolute(edge_kernel);
	else if (ans.substr(0, 1) == "b")
		a.kernel_convolute(blur_kernel);
	else if (ans.substr(0, 1) == "s")
		a.kernel_convolute(sharpen_kernel);

	a.create_file();
	return 0;
}