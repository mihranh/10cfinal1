# BMP Convolution Image Filter #

   My final project application deals with reading, filtering, and writing **24-bit BMP image files.** The class BMPimg can read and store pixel data from a file. The various member functions help to add filters to the images by manipulating this pixel data.

   This project is inspired by my major in Psychology where I have learned about convolution neural networks. Like the human visual cortex, convolution filtering works by comparing each datum to surrounding data. This program generates convolution filters by modifying each pixel of an image to represent the average values of the 24 pixels surrounding it, weighed by a kernel matrix. Different kernel matrices provide for different effects.

[EXAMPLE IMAGES](http://imgur.com/a/PBsze)

## To use this program: ##
1. Compile the code with a **24-bit .bmp image file** in the same folder
2. Enter the name of the image file when prompted
3. select which color filter and/or convolution filter you wish to apply
4. filtered image will be generated in folder (can take a while)

### Convolution Kernel Filters ###
* Blur
* Sharpen
* Edge Detect - my favorite as it most accurately depicts how our brains
search for borders in our visual field to distinguish objects.

### NOTES ###
* **MUST use a 24-bit .bmp image file!**
* Color filters were mainly added so that the code could demonstrate use of algorithms and lambda functions. While they work well, I suggest trying them independently from the convolution filters as the dramatic color filters can disguise the convolution filters.
* The Edge Detect filter inverts colors, so if you apply a color filter then this filter, you will get the inverted color filter.